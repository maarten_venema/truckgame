var expect = chai.expect;
var assert = chai.assert;

describe('Formatting', function() {
	describe('.mToKm', function() {
		it('should return an integer', function() {
			var result = Formatting.mToKm(1000);
			expect(result).to.be.a('number');
			expect(result % 1).to.be.equal(0);
		});
		it('should return 1 as an integer when when we give it 1000 meters', function() {
			var result = Formatting.mToKm(1000);
			expect(result).to.equal(1);
		});
		it('should return 2 as an integer when when we give it 1500 meters (rounding up)', function() {
			var result = Formatting.mToKm(1000);
			expect(result).to.equal(1);
		});
		it('should return 1 as an integer when when we give it 1400 meters (rounding down)', function() {
			var result = Formatting.mToKm(1000);
			expect(result).to.equal(1);
		});
		it('when we input a string containing letters it should return NaN', function() {
			var result = Formatting.mToKm(0);
			expect(result).to.be.NaN;
		});
	});

	describe('.money', function() {
		it('when we input 123456.789 as a number its should return a formatted string representing money(EUR)', function() {
			var result = Formatting.twoDigits(4214142.214);
			expect(result).to.equal('4.214.142,21');
		});
		it('when we input a string containing comma\'s it should return NaN', function() {
			var result = Formatting.twoDigits(parseFloat('aaa4214142,214'));
			expect(result).to.be.NaN;
		});
		it('when we input a string containing letters or other other characters it should return NaN', function() {
			var result = Formatting.twoDigits(parseFloat('aaa4214142.214'));
			expect(result).to.be.NaN;
		});
	});

	describe('.twoDigits', function() {
		it('when we input the sum of 3*3.5 it should return an european formatted string using a comma instead of a dot with two trailing digits', function() {
			var result = Formatting.twoDigits(3*3.5);
			expect(result).to.equal('10,50');
		});
		it('when we input a string containing letters it should return NaN', function() {
			var result = Formatting.twoDigits(parseFloat('432garbage21'));
			expect(result).to.be.NaN;
		});
	});

	describe('.noDigits', function() {
		it('when we input the sum of 3*3.5 it should return an integer', function() {
			var result = Formatting.noDigits(3*3.5);
			expect(result).to.be.a('number');
			expect(result % 1).to.be.equal(0);
		});
		it('when we input the sum of 3*3.5 it should return 11 (rounding up)', function() {
			var result = Formatting.noDigits(3*3.5);
			expect(result).to.equal(11);
		});
		it('when we input the sum of 3*3.5 it should return 10 (rounding down)', function() {
			var result = Formatting.noDigits(3*3.2);
			expect(result).to.equal(10);
		});
		it('when we input a string containing letters it should return NaN', function() {
			var result = Formatting.noDigits(parseFloat('432garbage21'));
			expect(result).to.be.NaN;
		});
	});

	describe('.timestampToDate.format', function() {
		it('when we input 1420113600000(integer) it should return "1-1-2015 13:00" which is a formatted(EUR) string', function() {
			// var result = Formatting.timestampToDate.format();
			var result = Formatting.timestampToDateFormatter.format(1420113600000);
			expect(result).to.equal('1-1-2015 13:00');
			expect(result).to.be.a('string');
		});

		it('when we input a date object with the value 1420113600000 it should return "1-1-2015 13:00" which is a formatted(EUR) string', function() {
			// var result = Formatting.timestampToDate.format();
			var result = Formatting.timestampToDateFormatter.format(new Date(1420113600000));
			expect(result).to.equal('1-1-2015 13:00');
			expect(result).to.be.a('string');
		});
		it('when we input a random string it should return an error', function() {
			// var result = Formatting.timestampToDate.format();
			var result = Formatting.timestampToDateFormatter.format('432garbage21');
			expect(result).to.be.an('error');
			console.log(result);
		});	
	});
});