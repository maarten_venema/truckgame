class Stats {
    public now = {};
    public previous = [];
    private startTime = null;
    private day = 1;

    private update(): void {
        this.now.time = Time.time;
        this.now.money = Financial.playerMoney;
        this.now.moneyNegative = this.now.money < 0;

        if(this.startTime === null) {
            this.startTime = this.now.time;
            this.now.day = this.day;

            let now = Object.assign({},this.now)
            this.previous.push(now);
        } else if(this.startTime !== null && this.now.time > this.startTime+(this.day*86400) && this.previous.length == this.day) {
            this.day = this.day+1;
            this.now.day = this.day;

            window.dispatchEvent( new Event('daySwitch') );

            let now = Object.assign({},this.now)
            this.previous.push(now);
        }
    }
}