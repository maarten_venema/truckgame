var Route = {};

// Generate a random route
Route.generateRoute = function(truck, job){
	// Get a random destination
	do {
		var randomDestination = Route.generateRandomLocation(truck.baseLocation, truck.range);
	}
	while (Route.locationAboveWater(randomDestination));

	// Format a route request
	var request = {
			origin:			truck.baseLocation,
			waypoints:		[{
								location: randomDestination,
								// stopover: false
							}],
			destination:	truck.baseLocation,
			travelMode: 	google.maps.TravelMode.DRIVING,
			unitSystem: 	google.maps.UnitSystem.METRIC
		};

	// Request a route
	Game.directionService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK && result.routes[0].legs[0].steps.length > 0) {

			// Check if ends on water = ferry SHOULD ADD: also check if length is too long
			let lastStep = result.routes[0].legs[0].steps.length-1;
			// console.log((result.routes[0].legs[0].distance.value/1000));
			if(result.routes[0].legs[0].steps[lastStep].maneuver=='ferry') {
				setTimeout(function(){ Route.generateRoute(truck); }, 1000);
				return;
			}

			var prevLatLng = null;
			var endStepSet = false;
			var route = {};
			route.path = [];
			polylinePath = [[],[]];
			let ferrySteps = 0;
			
			// Interpolate the route steps so that there is no more then 10 meters distance between the steps
			for (i = 0; i < result.routes[0].legs.length; i++){
				for (step of result.routes[0].legs[i].steps){
					let ferry = step.maneuver=='ferry' ? true:false;
					for (LatLng of step.lat_lngs) {

						if(i==1 && !route.endStep){
							route.endStep = route.path.length;
							endStepSet = true;
						}

						if(prevLatLng && google.maps.geometry.spherical.computeDistanceBetween(prevLatLng, LatLng) > 20){
							var distance = google.maps.geometry.spherical.computeDistanceBetween(prevLatLng, LatLng);
							var fractions = Math.floor(distance/20);
							if(fractions > 1){
								for(fi = 0; fi < fractions; fi++){
									var step = 1/fractions;
									var interpolatedLatLng = google.maps.geometry.spherical.interpolate(prevLatLng, LatLng, step * fi);
									route.path.push({latLng: interpolatedLatLng, ferry: ferry});

									ferrySteps = ferry==true ? ferrySteps+1:ferrySteps;
								}
							}
						}

						ferrySteps = ferry==true ? ferrySteps+1:ferrySteps;
						prevLatLng = LatLng;
						route.path.push({latLng:LatLng, ferry: ferry});
						polylinePath[i].push(LatLng);
					}
				}
			}

			// Set other route values
			route.polylinePath	 = polylinePath;
			route.startAdress 	= result.routes[0].legs[0].start_address;
			route.startLatLng 	= result.routes[0].legs[0].start_location;
			route.endAdress 	= result.routes[0].legs[0].end_address;
			route.endLatLng 	= result.routes[0].legs[0].end_location;
			route.distance 		= result.routes[0].legs[0].distance;
			route.duration 		= result.routes[0].legs[0].duration;

			// Calculate the number of meters per step
			route.meterPerstep	= route.distance.value / route.path.length;

			// Calculate the amount of fuel used per step
			route.fuelPerstep	= route.meterPerstep * (((100/truck.fuelEfficiency)*0.01)/1000);

			// Set break and sleep times for this route
			truck.breakTime = Time.time+(settings.breakInterval*60*60);
			if(truck.sleepTime < Time.time){
				truck.sleepTime = Time.time+(settings.sleepInterval*60*60);
			}

			// ETA - Estimate Time Of Arrival
			var distance = route.distance.value/1000;
			var basicEta = distance/(truck.speed);

			var timesSleeping = Math.round(basicEta / settings.sleepInterval);
			var timesOnBreak = Math.round(basicEta / settings.breakInterval) - timesSleeping;
			var timeSpendSleeping = timesSleeping * settings.sleepDuration;
			var timeSpendOnBreak = timesOnBreak * settings.breakDuration;
			var eta = basicEta + timeSpendSleeping + timeSpendOnBreak;
			eta = eta+(eta*0.008);
			route.eta = Math.round(Time.time + (eta*60*60));
			route.etaBack = Math.round(Time.time + ((eta+eta)*60*60));

			// Adjust the reward according to the lenght of the route
			truck.job.reward = Math.round(truck.job.reward*distance)+1000;
			
			// Set the route, set the the truck to the 'driving to delivery adress' state so it will start driving and reset the job paid state
			truck.job.route = route;
			truck.routeStep = 0;
			truck.job.paid = 0;
			truck.state = 'waiting for driver';

		} else {
			setTimeout(function(){ Route.generateRoute(truck); }, 1000);
		}
	});
}

// Generate a random location in a radius(in km) around given lat long
Route.generateRandomLocation = function(origin, range){
	return destination = origin.destinationPoint(Math.floor((Math.random() * 360)), Math.floor(range-(Math.random() * 100)+(Math.random() * 100)));
}

// Check if the given lat long is above (most) sea/lake levels
Route.locationAboveWater = function(latlongObj){
	var positionalRequest = {
		locations: [latlongObj]
	}

	Game.elevationService.getElevationForLocations(positionalRequest, function(result, status) {
		if (status == google.maps.ElevationStatus.OK) {
			if (result[0] && result[0].elevation >-10) {
				return true;
			}
		}
	});

	return false;
}