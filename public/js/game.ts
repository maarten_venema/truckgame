class Game {
	private state = 'loading';
	private runtimeWorker = null;
	private runtimeInterval = null;

	// Google APIs
	public mainMap = null;
	public directionService = null;
	public directionsDisplay = null;
	public elevationService = null;

	// Game object to intialize
	public load = ['Company','Banks','Stats','Garages','Gui'];

	// Game objects
	public objects = [];
	private updateObjects = [];
	private drawObjects = [];

	constructor() {
		let self = this;

		Template.initialize();

		// Initialize map and related services
		// We dont want company information on the map
		let noPoi = [
			{
				featureType: "poi",
				stylers: [
					{ visibility: "off" }
				]
			}
		];

		let mapOptions = {
			center: { lat: settings.lat, lng: settings.lng},
			zoom: 8,
			disableDefaultUI: true,
			styles: noPoi,
			gestureHandling: 'greedy'
		};

		/* Init map and google api services */
		self.mainMap = new google.maps.Map(document.getElementById('main-map'), mapOptions);
		self.directionService = new google.maps.DirectionsService();
		self.directionsDisplay = new google.maps.DirectionsRenderer();
		self.elevationService = new google.maps.ElevationService();

		// Map click
		google.maps.event.addListener(self.mainMap, 'click', function(event){
			if(self.objects.Garages.buying){
				window.Game.objects.Garages.create(event);
			}
		});

		// Initialize game objects
		for (var i = self.load.length - 1; i >= 0; i--) {
			let gameObj = new window[self.load[i]]();
			self.registerGameObj(gameObj, self.load[i], typeof gameObj.update === "function", typeof gameObj.draw === "function", typeof gameObj.bind === "function");
		}

		// Start time
		if(window.Worker){
			self.runtimeWorker = new Worker("/js/runtimeWorker.js");
			self.runtimeWorker.postMessage(settings.fps);

			self.runtimeWorker.onmessage = function(e) {
				self.runtime();
			}
		} else {
			self.runtimeInterval = setInterval(self.runtime, 1000 / settings.fps);
		}

		self.state = 'running';


		// Make this available in class as self
		window.Game = self;

		test();
    }

    private registerGameObj = function(gameObj:object, objName:string, hasUpdate:boolean, hasDraw:boolean, hasBind:boolean) {
    	self.objects[objName] = gameObj;

    	if(hasUpdate) { 
    		self.updateObjects[objName] = gameObj; 
    	}

    	if(hasDraw) { 
    		self.drawObjects[objName] = gameObj; 
    	}

    	if(hasBind) { 
			setTimeout(function(){
				gameObj.bind();
			}, 500);
    	}
    }

	private runtime = function(){
		if(self.state == 'running'){
			self.update();
			self.draw();

			// Temp draggable and resizeable fix (hacky)
			$('.window:not(.ui-draggable)').draggable({handle: ".topbar", snap: ".window", snapMode: "outer", containment: "document" }).resizable({maxHeight: 500, maxWidth: 800, minHeight: 250, minWidth: 550});;
		}
	};

	private update = function(){
		window.Time.updateTime();
		window.Trucks.updateTrucks();
		window.Staff.updateStaff();

		for(let obj in self.updateObjects) {
			self.updateObjects[obj].update();
		}

		document.title = 'Truck | '+Formatting.timestampToDateFormatter.format(new Date(Time.time*1000))+' | '+Formatting.money(Financial.playerMoney);
	}

	private draw = function(){
		window.Trucks.drawTrucks();

		for(let obj in self.drawObjects) {
			self.drawObjects[obj].update();
		}
	}
}

window.addEventListener('daySwitch', function (e) {
	Staff.payStaff();
	Game.objects.Banks.updateDay();
});

window.onload = function () { new Game() };

// Simple function for testing purposes
var test = function(){


	let latLongs = [
		{lat:53.049503, long:4.791356},
		{lat:52.820778, long:6.180054},
		{lat:52.407198, long:5.982300},
		{lat:51.989705, long:5.059449},
		{lat:51.266817, long:5.916382},
		{lat:51.032503, long:3.609253},
		{lat:53.006276, long:8.816773},
		{lat:52.407198, long:9.761597},
		{lat:51.458888, long:0.104126},
		{lat:49.701129, long:6.136109},
		{lat:55.303765, long:10.395514},
		{lat:53.273069, long:-2.858533},
		{lat:54.234843, long:18.745425},
		{lat:55.591272, long:13.076388},
		{lat:48.845232, long:2.507861},
		{lat:47.199453, long:-1.491162},
		{lat:39.479160, long:-6.034733},
		{lat:59.784413, long:48.105892,
		{lat:48.105892, long:25.254330},
		{lat:64.380381, long:16.113705}
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:},
		// {lat:, long:}

	];

	for (var i = latLongs.length - 1; i >= 0; i--) {

	}

	for (var i = latLongs.length - 1; i >= 0; i--) {
	    (function(n) {
	        setTimeout(function(){
	        	Financial.addPlayerMoney(300000);

	            let latLong = latLongs[n];

	            let obj = {};
				obj.latLng = new google.maps.LatLng(latLong.lat, latLong.long);

				let orgLatLng = obj.latLng;
				Game.objects.Garages.buyingGarageType = {
					"name": "Small garage",
					"icon": "/img/icons/map-garage.png",
					"price": 150000,
					"upkeep": 1000,
					"truckSpace": 5
				};

				Game.objects.Garages.create(obj);

	            (function(n) {
			        setTimeout(function(){
			            var garage = Game.objects.Garages.garages[n];
						var person = Staff.createPerson();
						Staff.staff.push(person);
						Game.objects.Garages.garages[n].staff.push(person);

						var person2 = Staff.createPerson();
						Staff.staff.push(person2);
						Game.objects.Garages.garages[n].staff.push(person2);

						var truckStats = settings.truckTypes['DAF'][0];
						truckStats.garageID = garage.id;
						truckStats.brand = 'DAF';
						truckStats.baseLocation = orgLatLng;
						Trucks.createTruck(truckStats);
			        }, 2000);
			    }(n));

	        }, 1000*i);
	    }(i));
	}
};
// 	for (var i = latLongs.length - 1; i >= 0; i--) {
// 		setTimeout(function(i){
// 			let latLong = latLongs[i];

// 			var obj = {};
// 			obj.latLng = new google.maps.LatLng(latLong.lat, latLong.long);

// 			var orgLatLng = obj.latLng;
// 			Game.objects.Garages.buyingGarageType = {
// 				"name": "Small garage",
// 				"icon": "/img/icons/map-garage.png",
// 				"price": 150000,
// 				"upkeep": 1000,
// 				"truckSpace": 5
// 			}

// 			Game.objects.Garages.create(obj);

// 			setTimeout(function(i){
// 				var garage = Game.objects.Garages.garages[i];

// 				var person = Staff.createPerson();
// 				Staff.staff.push(person);
// 				Game.objects.Garages.garages[i].staff.push(person);

// 				var person2 = Staff.createPerson();
// 				Staff.staff.push(person2);
// 				Game.objects.Garages.garages[i].staff.push(person2);

// 				var truckStats = settings.truckTypes['DAF'][0];
// 				truckStats.garageID = garage.id;
// 				truckStats.brand = 'DAF';
// 				truckStats.baseLocation = orgLatLng;
// 				Trucks.createTruck(truckStats);
// 			}, 200, i);
// 		}
// 	}, 2000, i);
// }