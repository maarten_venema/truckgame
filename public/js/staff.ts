var Staff = {};
Staff.staff = [];
Staff.hireableStaff = [];
Staff.poolRefresh = 0;
Staff.xpPenalty = 40; // standard penalty to keep the experience balanced in relation to the age
Staff.xpBonus = 0;

Staff.updateStaff = function(){
	if(Staff.hireableStaff.length < 5) {
		Staff.createStaffPool();
	}
}

Staff.payStaff = function(){
	for (var i = this.staff.length - 1; i >= 0; i--) {
		let staffMember = this.staff[i];

		Financial.subtractPlayerMoney(staffMember.pay);
	}
}

Staff.createStaffPool = function(){
	var amount = Math.floor(((Math.random() * 10) + 1)+10);
	for (i = 0; i < amount; i++) { 
		Staff.hireableStaff.push(Staff.createPerson());
	}
}

Staff.createPerson = function(){
	// Person
	var person = {};
	var gender = chance.gender();
	person.name = chance.name({ gender: gender });
	person.gender = gender;
	person.age = chance.age({type: 'adult'});

	// Experience a person has. Its based on the age - a penalty (keeps it a bit more random) + bonus (upgrades)
 	var experience = (Math.floor((Math.random() * 25) + 1)-(Staff.xpPenalty-person.age))+Staff.xpBonus;
	experience = person.age - experience >= 18 ? experience : (experience - (person.age - experience)); 
	person.training = [];
	person.experience = experience < 0 ? 0 : experience;

	// Pay per month. based on experience but never more then 2500,-
	var pay = 1400+(person.experience*40);
	person.pay = person.pay > 2500 ? 2500:pay;

	person.state = 'waiting for job';

	// default the window focus to 0/not visible
	person.windowFocus = 0;

	return person;
}

Staff.hire = function(personId, garageId) {
	garage = Game.objects.Garages.garages[garageId];
	var personObj = Staff.hireableStaff[personId];
	Staff.staff.push(personObj);
	garage.staff.push(personObj);
	Staff.hireableStaff.splice(personId, 1);

	var hireableStaff = '';
	for (i = 0; i < Staff.hireableStaff.length; i++) {
		person = Staff.hireableStaff[i];
	}
}

// Truck information window (overview)
Staff.openOverview = function(person){
	if($('.window.truck_'+truck.id).length == 0){
		var windowEl = Template.create('truck', truck);
		windowEl.resizable({maxHeight: 500, maxWidth: 800, minHeight: 250, minWidth: 550});
		windowEl.draggable({start: function(event, ui){UI.setWindowFocus(ui.helper[0]);}, handle: ".topbar", snap: ".window", snapMode: "outer" });
		$('body').append(windowEl);
		UI.setWindowFocus(windowEl);
		Staff.updateOverview(truck);

		/* Set upgrade info tab information */
		$('.truck_'+truck.id+' #upgrades_cargoTypes').append(truck.cargoUpgradesDOM);
		$('.truck_'+truck.id+' #upgrades_range').append(truck.rangeUpgradesDOM);

		for(var upgradeType in truck.upgrades){
			for(var upgrade in truck.upgrades[upgradeType]){
				if(truck.upgrades[upgradeType][upgrade].active===1){
					$('.truck_'+truck.id+' .upgrade[data-upgrade-type="'+upgradeType+'"][data-upgrade-name="'+upgrade+'"]').addClass('activated');
				}
			};
		}
		

	} else {
		var windowEl = '.truck_'+truck.id;
		UI.setWindowFocus(windowEl);
	}
}

Staff.updateOverview = function(person){
	var personData = {};

	for(var attribute in person){
		personData[attribute] = person[attribute];
	}

	Staff.drawOverview(personData);
}

Staff.focusOverview = function(person){
	staff.windowFocus = 1;
}

Staff.blurOverview = function(person){
	staff.windowFocus = 0;
}

Staff.destroyOverview = function(person){
	Staff.blurOverview(person);
}

Staff.drawOverview = function(person){
	Template.parse($('.staff_'+person.id), truck);
}