// Load templates
var Template = {};
Template.templates = [
							{'name':'truck','file':'truck-window.html', 'html':''},
							{'name':'garage', 'file':'garage-window.html', 'html':''},
							{'name':'company', 'file':'company-window.html', 'html':''},
							{'name':'bank', 'file':'bank-window.html', 'html':''}
					];

Template.initialize = function(){
	var requests = [];

	for (var template in Template.templates){

		if (Template.templates.hasOwnProperty(template)) {
			var file = Template.templates[template].file;
			requests.push($.ajax({type: 'GET', url: '/tmpl/'+file}));
		}

	}

	$.when.apply($, requests).done(function () {
		$.each(arguments, function (i, html) {
			Template.templates[i].html = html[0];
		});
	});
}

Template.create = function(name, data){
	for(var i = 0, m = null; i < Template.templates.length; ++i) {
		if(Template.templates[i].name == name) {
			m = Template.templates[i];
			break;
		}
	}

	var tmpl = Template.parse(m.html, data);
	tmpl = $(tmpl);
	$('body').append(tmpl)

	return tmpl;
}

Template.parse = function(tmpl, data){
	if(typeof tmpl == 'string'){
		for(var attribute in data){
			tmpl = tmpl.replace(new RegExp('{{'+attribute+'}}', 'g'), data[attribute]);
		}
	}
	if(typeof tmpl == 'object'){
		for(var attribute in data){
			var el = $(tmpl).find('* *[data-bind-to="'+attribute+'"]');
			var newVal = data[attribute];
			if(el.attr('data-format')){
				var formatter = el.data('format');
				newVal = Formatting[formatter](newVal);
			}
			el.html(newVal);
		}
	}

	return tmpl;
}

Template.destroy = function(el){
	var objectId = el.data("id");
	if(el.hasClass('truck')){
		Trucks.destroyOverview(Trucks.trucks[objectId]);
	} else if(el.hasClass('garage')) {
	}
	el.remove();
}