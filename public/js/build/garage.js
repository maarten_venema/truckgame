var Garages = (function () {
    function Garages() {
        this.buying = false;
        this.buyingGarageType = null;
        this.garages = [];
        self = this;
    }
    Garages.prototype.create = function (event) {
        stats = self.buyingGarageType;
        if (stats !== null && Financial.checkPlayerMoney(stats.price)) {
            stats.location = event.latLng;
            // Check if there is a road nearby if not dont create a garage
            var request = {
                origin: stats.location,
                destination: stats.location,
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC
            };
            Game.directionService.route(request, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    // Road found
                    var garage_1 = {};
                    garage_1.id = Game.objects.Garages.garages.length;
                    for (var stat in stats) {
                        garage_1[stat] = stats[stat];
                    }
                    garage_1.location = result.routes[0].legs[0].steps[0].lat_lngs[0];
                    garage_1.name += ' ' + (garage_1.id + 1);
                    garage_1.marker = new google.maps.Marker({
                        position: garage_1.location,
                        map: Game.mainMap,
                        icon: garage_1.icon,
                        zIndex: google.maps.Marker.MAX_ZINDEX + 1,
                        title: garage_1.name
                    });
                    // no opened window is no focus
                    garage_1.windowFocus = 0;
                    google.maps.event.addListener(garage_1.marker, 'click', function () {
                        Game.objects.Garages.openOverview(garage_1);
                    });
                    garage_1.trucks = [];
                    garage_1.staff = [];
                    Game.objects.Garages.garages.push(garage_1);
                    // Substract garage cost from player money
                    Financial.subtractPlayerMoney(garage_1.price);
                }
                else {
                    // No road found
                    console.log('wrong location');
                }
            });
        }
    };
    Garages.prototype.openOverview = function (garage) {
        var id = garage.id;
        if ($('.window.garage_' + id).length < 1) {
            var windowEl = Template.create('garage', { id: id });
            $('body').append(windowEl);
            Game.objects.Gui.setWindowFocus(windowEl);
            var view = new Vue({
                el: '.window.garage_' + id,
                data: {
                    garage: garage,
                    availableTrucks: settings.truckTypes,
                    staffpool: Staff.hireableStaff
                },
                methods: {
                    buyTruck: function (brand, garageId, truck) {
                        var garage = Game.objects.Garages.garages[garageId];
                        var truckStats = truck;
                        // Check if the player has enough money to buy this truck
                        if (Financial.checkPlayerMoney(truckStats.price)) {
                            truckStats.garageID = garageId;
                            truckStats.brand = brand;
                            truckStats.baseLocation = garage.location;
                            Trucks.createTruck(truckStats);
                        }
                    },
                    hireStaff: function (personId, garageId) {
                        Staff.hire(personId, garageId);
                    }
                }
            });
        }
    };
    return Garages;
}());
// var Garages = {};
// Garages.buyEl = '#buy-garage';
// Garages.buying = false;
// Garages.overviewEl = document.getElementById('garage');
// Garages.overviewActive = false;
// Garages.activeGarage= null;
// Garages.garages = [];
// Function to create new garage objects
// Garages.createGarage = function(stats){
// 	// Check if there is a road nearby if not dont create a garage
// 	var request = {
// 			origin:			stats.location,
// 			destination:	stats.location,
// 			travelMode: 	google.maps.TravelMode.DRIVING,
// 			unitSystem: 	google.maps.UnitSystem.METRIC
// 		};
// 	Game.directionService.route(request, function(result, status) {
// 		if (status == google.maps.DirectionsStatus.OK) {
// 			// Road found
// 			var garage = {};
// 			garage.id = Garages.garages.length;
// 			for(var stat in stats){
// 				garage[stat] = stats[stat];
// 			}
// 			garage.location = result.routes[0].legs[0].steps[0].lat_lngs[0];
// 			garage.name += ' '+(garage.id+1);
// 			garage.marker = new google.maps.Marker({
// 								position: garage.location,
// 								map: Game.mainMap,
// 								icon: garage.icon,
// 								zIndex: google.maps.Marker.MAX_ZINDEX + 1,
// 								title: garage.name
// 							});
// 			// no opened window is no focus
// 			garage.windowFocus = 0;
// 			google.maps.event.addListener(garage.marker, 'click', function() {
// 				Garages.openOverview(garage);
// 			});
// 			garage.trucks	= [];
// 			garage.staff	= [];
// 			Garages.garages.push(garage);
// 			// Substract garage cost from player money
// 			Financial.subtractPlayerMoney(garage.price);
// 		} else {
// 			// No road found
// 			console.log('wrong location');
// 		}
// 	});
// }
// // Update all garage objects
// Garages.updateGarages = function(){
// 	// for (garage of Garages.garages) {
// 	// }
// }
// // update graphical representation of all garages
// Garages.drawGarages = function(){
// 	for (garage of Garages.garages) {
// 		// Update overview if its visible (opened)
// 		if($('.garage'+garage.id).is(":visible")){
// 			Garages.updateOverview(garage);
// 		}
// 	}	
// }
// // open the overview of a garage
// Garages.openOverview = function(garage,byId){
// 	byId = (typeof byId === 'undefined') ? 0 : byId;
// 	if(byId) {
// 		garage = Garages.garages[garage];
// 	}
// 	if($('.garage_'+garage.id).length == 0){
// 		var windowEl = Template.create('garage', garage);
// 		windowEl.resizable({maxHeight: 1000, maxWidth: 1000, minHeight: 250, minWidth: 550});
// 		windowEl.draggable({start: function(event, ui){UI.setWindowFocus(ui.helper[0]);}, handle: ".topbar", snap: ".window", snapMode: "outer" });
// 		$('body').append(windowEl);
// 		Garages.updateOverview(garage);
// 		UI.setWindowFocus(windowEl);
// 	} else {
// 		UI.setWindowFocus('.garage_'+garage.id);
// 	}
// }
// // Update statistisc to active garage windows
// Garages.updateOverview = function(garage){
// 	if($('.window.garage_'+garage.id).length != 0){
// 		data = {};
// 		var buyableTrucks = '';
// 		for(var brand in settings.truckTypes){
// 			buyableTrucks += '<div class="brand">'+brand+'</div>';
// 			for(var truck in settings.truckTypes[brand]){
// 				buyableTrucks += '<div class="truck" data-garage="'+garage.id+'" data-brand="'+brand+'" data-type="'+truck+'" onclick="Trucks.buyTruck(this);">'+settings.truckTypes[brand][truck].type+'</div>';
// 			}
// 		}
// 		data['buyableTrucks'] = buyableTrucks;
// 		var hireableStaff = '';
// 		for (i = 0; i < Staff.hireableStaff.length; i++) {
// 			person = Staff.hireableStaff[i];
// 			hireableStaff += '<div class="person" data-garage="'+garage.id+'" data-id="'+i+'" onclick="Staff.hire(this);"><b>'+person.name+'</b><br>age: '+person.age+'<br>sex: '+person.gender+'<br>experience: '+person.experience+' years<br>pay per month: '+Formatting.money(person.pay)+'<br><br></div>';
// 		}
// 		data['hireableStaff'] = hireableStaff;
// 		var hiredStaff = '';
// 		for (i = 0; i < garage.staff.length; i++) {
// 			staffMember = garage.staff[i];
// 			hiredStaff += '<div class="person"><b>'+staffMember.name+'</b><br>age: '+staffMember.age+'<br>sex: '+staffMember.gender+'<br>experience: '+staffMember.experience+' years<br>pay per month: '+Formatting.money(staffMember.pay)+'<br><br></div>';
// 		}
// 		data['hiredStaff'] = hiredStaff;
// 		var allTrucks = '';
// 		for (i = 0; i < garage.trucks.length; i++) {
// 			truck = garage.trucks[i];
// 			allTrucks += '<div class="truck"><b>'+truck.name+'</b><br>type: '+truck.type+'<br><br></div>';
// 		}
// 		data['allTrucks'] = allTrucks;
// 		for(var attribute in garage){
// 				data[attribute] = garage[attribute];
// 		}
// 		Template.parse($('.garage_'+garage.id), data);
// 	}
// }
// Garages.focusOverview = function(garage){
// 	garage.windowFocus = 1;
// }
// Garages.blurOverview = function(garage){
// 	garage.windowFocus = 0;
// }
// Garages.drawOverview = function(){
// 	for (garage of Garages.garages) {
// 		if($('.garage_'+garage.id).length){
// 			Garages.updateOverview(garage);
// 		}
// 	}	
// }
// Garages.destroyOverview = function(garage){
// 	garage.windowFocus = 0;
// }
// // Hire staff
// Garages.hireStaff = function(){
// }
// // Buy garage
// Garages.buyGarage = function(){
// 	Garages.buying = !Garages.buying;
// 	(Garages.buying) ? $(Garages.buyEl).addClass('active') : $(Garages.buyEl).removeClass('active');
// }
// $(Garages.buyEl).click(Garages.buyGarage);
// /* All garages overview */
// Garages.openGaragesOverview = function(){
// 	if($('#garage_overview').length == 0){
// 		var garagesData = {};
// 		garagesData.garages = '<ul>';
// 		for (garage of Garages.garages) {
// 			garagesData.garages += '<li>'+garage.name+'</li>';
// 		}
// 		garagesData.garages += '</ul>';
// 		var windowEl = Template.create('garageOverview', garagesData);
// 		windowEl.resizable({maxHeight: 500, maxWidth: 800, minHeight: 250, minWidth: 550});
// 		windowEl.draggable({start: function(event, ui){UI.setWindowFocus(ui.helper[0]);}, handle: ".topbar", snap: ".window", snapMode: "outer" });
// 		$('body').append(windowEl);
// 		UI.setWindowFocus(windowEl);
// 	} else if($('#garage_overview').is(":hidden")){
// 		UI.setWindowFocus('#garage_overview');
// 	} else {
// 		UI.setWindowFocus('#garage_overview');
// 	}
// } 
