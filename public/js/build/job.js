var Job = {};
Job.generateJob = function (truck) {
    var cargo = Job.generateCargo(truck.cargoTypes);
    var weight = truck.cargo - randomIntFromInterval(1, 2) + randomIntFromInterval(1, 2);
    truck.job.cargo.name = cargo.name;
    truck.job.cargo.weight = weight;
    // per tonne per km. Route.js > generateRoute() will adjust this to the correct value. Should use promises so i can do this more cleanly
    truck.job.reward = weight * cargo.price;
    var route = Route.generateRoute(truck);
};
Job.generateCargo = function (cargoTypes) {
    var cargoType = cargoTypes[Math.floor(Math.random() * cargoTypes.length)];
    var cargo = settings.cargoTypes[cargoType][Math.floor(Math.random() * (settings.cargoTypes[cargoType].length - 1))];
    return cargo;
};
