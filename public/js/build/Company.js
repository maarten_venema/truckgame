var Company = (function () {
    function Company() {
        this.view = null;
        this.name = 'Company';
    }
    Company.prototype.update = function () {
    };
    Company.prototype.openOverview = function () {
        if ($('#company').is(":hidden") || $('#company').length == 0) {
            // company.openOverview();
            if ($('#company').length == 0) {
                var windowEl = Template.create('company');
                $('body').append(windowEl);
                Game.objects.Gui.setWindowFocus(windowEl);
            }
            else if ($('#company').is(":hidden")) {
                Game.objects.Gui.setWindowFocus('#company');
            }
            else {
                Game.objects.Gui.setWindowFocus('#company');
            }
            this.view = new Vue({
                el: '#company',
                data: {
                    company: this,
                    stats: Game.objects.Stats.previous,
                    moneyHistory: Financial.moneyHistory
                },
                methods: {
                    moneyFunc: function (value) {
                        return Formatting.money(value);
                    }
                }
            });
        }
        else {
            //unbind.bind($('#bank'))
            Template.destroy($('#company'));
        }
    };
    return Company;
}());
