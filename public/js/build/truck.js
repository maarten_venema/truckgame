var Trucks = {};
Trucks.trucks = [];
Trucks.buyTruck = function (truck) {
    truck = $(truck);
    var garageID = truck.data('garage');
    garage = Game.objects.Garages.garages[garageID];
    var brand = truck.data('brand');
    var type = truck.data('type');
    var truckStats = settings.truckTypes[brand][type];
    // Check if the player has enough money to buy this truck
    if (Financial.checkPlayerMoney(truckStats.price)) {
        truckStats.garageID = garageID;
        truckStats.brand = brand;
        truckStats.baseLocation = garage.location;
        Trucks.createTruck(truckStats);
    }
    else {
        console.log('not enough money');
    }
};
// Function to create new truck objects
Trucks.createTruck = function (stats) {
    var truck = {};
    truck = jQuery.extend(true, {}, stats, settings.truck);
    truck.id = Trucks.trucks.length;
    truck.name = 'Truck ' + (truck.id + 1);
    truck.marker = new google.maps.Marker({
        position: truck.baseLocation,
        map: Game.mainMap,
        icon: truck.icon,
        title: truck.name
    });
    truck.deliveryMarker = new google.maps.Marker({
        position: truck.baseLocation,
        icon: settings.markerIcons.delivery,
        title: 'delivery adress'
    });
    truck.location = { latLng: truck.baseLocation };
    truck.fuel = truck.fuelCapicity;
    // Liters of fuel to be left before refilling
    truck.refuel = Math.floor(truck.fuelCapicity - (truck.fuelCapicity / (Math.floor(Math.random() * 3) + 2)));
    // Set the steps the truck has to drive per frame to drive semi realistic (timewise) 
    truck.stepsPerFrame = Trucks.calcStepsPerFrame(truck);
    truck.job = {};
    truck.job.driver = 0;
    truck.job.cargo = {};
    truck.job.route = {};
    truck.job.paid = 0;
    Job.generateJob(truck);
    // default the window focus to 0/not visible
    truck.windowFocus = 0;
    google.maps.event.addListener(truck.marker, 'click', function () {
        Trucks.openOverview(truck);
    });
    Game.objects.Garages.garages[truck.garageID].trucks.push(truck);
    Trucks.trucks.push(truck);
    // create `s DOM
    truck.cargoUpgradesDOM = '';
    for (upgrade in truck.upgrades.cargoTypes) {
        truck.cargoUpgradesDOM += '<li class="upgrade" data-upgrade-type="cargoTypes" data-upgrade-name="' + upgrade + '" data-upgrade-cost="' + truck.upgrades.cargoTypes[upgrade].cost + '">' + truck.upgrades.cargoTypes[upgrade].description + " - " + Formatting.money(truck.upgrades.cargoTypes[upgrade].cost) + '</li>';
    }
    truck.rangeUpgradesDOM = '';
    for (upgrade in truck.upgrades.range) {
        truck.rangeUpgradesDOM += '<li class="upgrade" data-upgrade-type="range" data-upgrade-name="' + upgrade + '" data-upgrade-cost="' + truck.upgrades.range[upgrade].cost + '">' + truck.upgrades.range[upgrade].description + "km - " + Formatting.money(truck.upgrades.range[upgrade].cost) + '</li>';
    }
    // Substract truck price from player money
    Financial.subtractPlayerMoney(truck.price);
    return truck.id;
};
// Update all truck objects
Trucks.updateTrucks = function () {
    for (var _i = 0, _a = Trucks.trucks; _i < _a.length; _i++) {
        truck = _a[_i];
        try {
            var ferry = truck.job.route.path[truck.routeStep].ferry;
        }
        catch (e) {
            return;
        }
        if (ferry) {
            truck.marker.setIcon(truck.iconBoat);
        }
        else {
            truck.marker.setIcon(truck.icon);
        }
        var stepsPerFrame = ferry ? truck.stepsPerFrame / 4 : truck.stepsPerFrame;
        if (truck.sleepTime && Time.time >= truck.sleepTime && truck.state != 'sleeping' && truck.state != 'tanking') {
            // Go to sleep
            truck.prevState = truck.state;
            truck.breakTime = Time.time + (settings.sleepDuration * 60 * 60) + (settings.breakInterval * 60 * 60);
            truck.sleepTime = Time.time + (settings.sleepDuration * 60 * 60) + (settings.sleepInterval * 60 * 60);
            truck.wakeUpTime = Time.time + (settings.sleepDuration * 60 * 60);
            truck.state = 'sleeping';
        }
        else if (truck.breakTime && Time.time >= truck.breakTime && truck.state != 'taking a break' && truck.state != 'tanking') {
            // Take a break
            truck.prevState = truck.state;
            truck.breakTime = Time.time + (settings.breakDuration * 60 * 60) + (settings.breakInterval * 60 * 60);
            truck.wakeUpTime = Time.time + (settings.breakDuration * 60 * 60);
            if (truck.sleepTime - truck.breakTime <= 60 * 60) {
                truck.breakTime = null;
            }
            truck.state = 'taking a break';
        }
        else if (truck.fuel < truck.refuel && truck.state != 'tanking') {
            // Filling up the tank
            truck.prevState = truck.state;
            truck.fuel = Math.round(truck.fuel);
            truck.refuel = Math.floor((Math.random() * 200) + 70);
            truck.state = 'tanking';
        }
        else if (truck.state != 'tanking' && truck.state != 'sleeping' && truck.state != 'taking a break') {
            // set the previous state if it was not one of the specific resting/busy cases
            truck.prevState = truck.state;
        }
        switch (truck.state) {
            case 'waiting for job':
                truck.direction = null;
                if (truck.job.driver !== 0) {
                    truck.job.driver.state = "waiting for job";
                    truck.job.driver = 0;
                }
                break;
            case 'waiting for driver':
                truck.direction = null;
                if (truck.job.driver !== 0) {
                    truck.job.driver.state = "waiting for job";
                    truck.job.driver = 0;
                }
                for (var i = 0; i < Game.objects.Garages.garages[truck.garageID].staff.length; i++) {
                    if (Game.objects.Garages.garages[truck.garageID].staff[i].state === 'waiting for job') {
                        Game.objects.Garages.garages[truck.garageID].staff[i].state = "driving";
                        truck.job.driver = Game.objects.Garages.garages[truck.garageID].staff[i];
                        truck.state = 'driving to delivery adress';
                        break;
                    }
                }
                break;
            case 'driving to delivery adress':
                if (truck.routeStep === 0) {
                    truck.direction = 'to';
                    // Create or update the route polyline on the start of a new route
                    if (!truck.job.routePolylineTo) {
                        // Create route polyline to destination
                        truck.job.routePolylineTo = new google.maps.Polyline({
                            path: truck.job.route.polylinePath[0],
                            strokeColor: '#FF5722',
                            strokeWeight: 4
                        });
                        // Create route polyline back from destination
                        truck.job.routePolylineBack = new google.maps.Polyline({
                            path: truck.job.route.polylinePath[1],
                            strokeColor: '#b2b2b2',
                            strokeWeight: 4
                        });
                    }
                    else {
                        truck.job.routePolylineTo.setPath(truck.job.route.polylinePath[0]);
                        truck.job.routePolylineBack.setPath(truck.job.route.polylinePath[1]);
                        truck.job.updateRoutePolyline = 1;
                    }
                    truck.deliveryMarker.setPosition(truck.job.route.endLatLng);
                }
                if (truck.routeStep < truck.job.route.endStep - 60) {
                    truck.fuel -= truck.job.route.fuelPerstep * stepsPerFrame;
                    truck.routeStep += stepsPerFrame;
                    truck.metersDriven += truck.job.route.meterPerstep * stepsPerFrame;
                    truck.metersDrivenJob += truck.job.route.meterPerstep * stepsPerFrame;
                }
                else {
                    truck.routeStep++;
                    truck.fuel -= truck.job.route.fuelPerstep;
                    truck.metersDriven += truck.job.route.meterPerstep;
                    truck.metersDriven += truck.job.route.meterPerstep;
                    truck.metersDrivenJob += truck.job.route.meterPerstep;
                }
                if (truck.routeStep == truck.job.route.endStep) {
                    truck.state = 'unloading';
                }
                else {
                    truck.location = truck.job.route.path[truck.routeStep];
                }
                break;
            case 'unloading':
                truck.state = 'driving back to garage';
                break;
            case 'driving back to garage':
                truck.direction = 'back';
                if (truck.routeStep < (truck.job.route.path.length - 60)) {
                    truck.fuel -= truck.job.route.fuelPerstep * stepsPerFrame;
                    truck.routeStep += stepsPerFrame;
                    truck.metersDriven += truck.job.route.meterPerstep * stepsPerFrame;
                    truck.metersDrivenJob += truck.job.route.meterPerstep * stepsPerFrame;
                }
                else {
                    truck.routeStep++;
                    truck.fuel -= truck.job.route.fuelPerstep;
                    truck.metersDriven += truck.job.route.meterPerstep;
                    truck.metersDrivenJob += truck.job.route.meterPerstep;
                }
                if (truck.routeStep == truck.job.route.path.length) {
                    truck.state = 'waiting for job';
                    truck.metersDrivenJob = 0;
                    Job.generateJob(truck);
                }
                else {
                    truck.location = truck.job.route.path[truck.routeStep];
                }
                // Pay for the job in the first step of the backroute (after unloading)
                if (!truck.job.paid) {
                    Financial.addPlayerMoney(truck.job.reward);
                    truck.job.paid = 1;
                }
                break;
            case 'tanking':
                truck.fuel++;
                if (truck.fuel == truck.fuelCapicity) {
                    truck.state = truck.prevState;
                }
                break;
            case 'sleeping':
                if (Time.time >= truck.wakeUpTime) {
                    truck.state = truck.prevState;
                }
                break;
            case 'taking a break':
                if (Time.time >= truck.wakeUpTime) {
                    truck.state = truck.prevState;
                }
                break;
            case 'repair':
                break;
            default:
                break;
        }
    }
};
// Function to calculate the steps a trucks needs to do per frame 
Trucks.calcStepsPerFrame = function (obj) {
    var mPerS = obj.speed / 3.6;
    var metersPerCycle = settings.timePerCycle * mPerS;
    var stepsPerCycle = metersPerCycle / settings.avgMeterPerStep;
    console.log(Math.round(stepsPerCycle / settings.fps));
    return Math.round(stepsPerCycle / settings.fps);
};
// update graphical representation of all trucks
Trucks.drawTrucks = function () {
    for (var _i = 0, _a = Trucks.trucks; _i < _a.length; _i++) {
        truck = _a[_i];
        // Update overview if its visible (opened)
        if ($('.truck_' + truck.id).is(":visible")) {
            Trucks.updateOverview(truck);
        }
        switch (truck.state) {
            case 'waiting for job':
                break;
            case 'driving to delivery adress':
                truck.marker.setPosition(truck.location.latLng);
                break;
            case 'unloading':
                break;
            case 'driving back to garage':
                truck.marker.setPosition(truck.location.latLng);
                break;
            case 'sleeping':
                break;
            case 'taking a break':
                break;
            case 'repair':
                break;
            default:
                break;
        }
    }
};
// Truck upgrades
$('body').on('click', '.window.truck .upgrade', function (event) {
    event.stopPropagation();
    var truck = $(this).parents('.window').data('id');
    truck = Trucks.trucks[truck];
    var upgradeType = $(this).data('upgrade-type');
    var upgradeName = $(this).data('upgrade-name');
    var upgradeCost = $(this).data('upgrade-cost');
    if (Trucks.upgradeStat(truck, upgradeType, upgradeName, upgradeCost)) {
        $(this).addClass('activated');
    }
});
Trucks.upgradeStat = function (truck, type, name, upgradeCost) {
    switch (type) {
        case 'cargoTypes':
            if (truck.upgrades.cargoTypes[name].active === 0 && Financial.checkPlayerMoney(upgradeCost)) {
                truck.upgrades.cargoTypes[name].active = 1;
                var upgradeVal = truck.upgrades.cargoTypes[name].value;
                truck.cargoTypes.push(upgradeVal);
                Financial.subtractPlayerMoney(upgradeCost);
                return 1;
            }
            return 0;
        case 'range':
            if (truck.upgrades.range[name].active === 0 && truck.upgrades.range[name].value > truck.range && Financial.checkPlayerMoney(upgradeCost)) {
                truck.upgrades.range[name].active = 1;
                var upgradeVal = truck.upgrades.range[name].value;
                truck.range = upgradeVal;
                Financial.subtractPlayerMoney(upgradeCost);
                return 1;
            }
            return 0;
        default:
            break;
    }
};
// Truck information window (overview)
Trucks.openOverview = function (truck) {
    if ($('.window.truck_' + truck.id).length == 0) {
        var windowEl = Template.create('truck', truck);
        // windowEl.resizable({maxHeight: 500, maxWidth: 800, minHeight: 250, minWidth: 550});
        // windowEl.draggable({start: function(event, ui){Game.objects.Gui.setWindowFocus(Game.objects.Gui.helper[0]);}, handle: ".topbar", snap: ".window", snapMode: "outer" });
        $('body').append(windowEl);
        Game.objects.Gui.setWindowFocus(windowEl);
        Trucks.updateOverview(truck);
        /* Set upgrade info tab information */
        $('.truck_' + truck.id + ' #upgrades_cargoTypes').append(truck.cargoUpgradesDOM);
        $('.truck_' + truck.id + ' #upgrades_range').append(truck.rangeUpgradesDOM);
        for (var upgradeType in truck.upgrades) {
            for (var upgrade in truck.upgrades[upgradeType]) {
                if (truck.upgrades[upgradeType][upgrade].active === 1) {
                    $('.truck_' + truck.id + ' .upgrade[data-upgrade-type="' + upgradeType + '"][data-upgrade-name="' + upgrade + '"]').addClass('activated');
                }
            }
            ;
        }
    }
    else {
        var windowEl = '.truck_' + truck.id;
        Game.objects.Gui.setWindowFocus(windowEl);
    }
};
Trucks.updateOverview = function (truck) {
    var truckData = {};
    for (var attribute in truck) {
        truckData[attribute] = truck[attribute];
    }
    // General stats
    truckData['garageName'] = Game.objects.Garages.garages[truck.garageID].name;
    truckData['driverName'] = truck.job.driver !== 0 ? truck.job.driver.name : '-';
    // Route information
    truckData['job.cargo.name'] = truck.job.cargo.name;
    truckData['job.cargo.weight'] = truck.job.cargo.weight;
    truckData['job.cargo.price'] = truck.job.cargo.price;
    truckData['job.reward'] = truck.job.reward;
    truckData['job.route.distance.total'] = Formatting.mToKm(truck.job.route.distance.value);
    truckData['job.route.endAdress'] = truck.job.route.endAdress;
    // Calculate the approximate duration of the job
    if (truck.direction) {
        var eta = (truck.direction == 'to') ? truck.job.route.eta : truck.job.route.etaBack;
        eta = new Date(eta * 1000);
        truckData['job.route.duration.text'] = Formatting.timestampToDateFormatter.format(eta);
    }
    else {
        truckData['job.route.duration.text'] = '-';
    }
    Trucks.drawOverview(truckData);
};
Trucks.focusOverview = function (truck) {
    truck.windowFocus = 1;
    truck.job.updateRoutePolyline = 1;
};
Trucks.blurOverview = function (truck) {
    truck.windowFocus = 0;
    truck.job.routePolylineTo.setMap(null);
    truck.job.routePolylineBack.setMap(null);
    truck.deliveryMarker.setMap(null);
};
Trucks.destroyOverview = function (truck) {
    Trucks.blurOverview(truck);
};
Trucks.drawOverview = function (truck) {
    Template.parse($('.truck_' + truck.id), truck);
    if (truck.windowFocus && truck.job.updateRoutePolyline) {
        truck.job.routePolylineBack.setMap(Game.mainMap);
        truck.job.routePolylineTo.setMap(Game.mainMap);
        truck.deliveryMarker.setMap(Game.mainMap);
        truck.job.updateRoutePolyline = 0;
    }
};
/* All garages overview */
Trucks.listTrucks = function () {
    return Trucks.trucks;
};
Trucks.openTrucksOverview = function () {
    if ($('#truck_overview').length == 0) {
        var trucksData = {};
        trucksData.trucks = '<ul>';
        for (var _i = 0, _a = Trucks.trucks; _i < _a.length; _i++) {
            truck = _a[_i];
            trucksData.trucks += '<li>' + truck.name + '</li>';
        }
        trucksData.trucks += '</ul>';
        var windowEl = Template.create('truckOverview', trucksData);
        // windowEl.resizable({maxHeight: 1000, maxWidth: 1000, minHeight: 250, minWidth: 550});
        // windowEl.draggable({start: function(event, ui){Game.objects.Gui.setWindowFocus(Game.objects.Gui.helper[0]);}, handle: ".topbar", snap: ".window", snapMode: "outer" });
        $('body').append(windowEl);
        Game.objects.Gui.setWindowFocus(windowEl);
    }
    else if ($('#truck_overview').is(":hidden")) {
        Game.objects.Gui.setWindowFocus('#truck_overview');
    }
    else {
        Game.objects.Gui.setWindowFocus('#truck_overview');
    }
};
