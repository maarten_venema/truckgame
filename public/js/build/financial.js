var Financial = {};
Financial.playerMoney = settings.startingPlayerMoney;
Financial.playerMoneyLastStep = 0;
Financial.moneyHistory = [];
// Financial.updateFinancial = function(){
// 	Game.objects.Stats.money = Financial.playerMoney;
// }
// Financial.drawFinancial = function(){
// 	if(Financial.playerMoneyLastStep!=Financial.playerMoney){
// 		$('#playerMoney').html(Formatting.money(Financial.playerMoney));
// 		if(Financial.playerMoney <= 0){
// 			$('#playerMoney').addClass('negative');
// 		} else {
// 			$('#playerMoney').removeClass('negative');
// 		}
// 		Financial.playerMoneyLastStep=Financial.playerMoney;
// 	}
// }
Financial.updateMoneyHistory = function () {
    Financial.moneyHistory.push(Financial.playerMoney);
    if (Financial.moneyHistory.length > 31) {
        Financial.moneyHistory.shift();
    }
};
Financial.addPlayerMoney = function (amount) {
    Financial.playerMoney += amount;
    Financial.updateMoneyHistory();
};
Financial.subtractPlayerMoney = function (amount) {
    Financial.playerMoney -= amount;
    Financial.updateMoneyHistory();
};
Financial.checkPlayerMoney = function (amount) {
    if (Financial.playerMoney >= amount) {
        return true;
    }
    return false;
};
