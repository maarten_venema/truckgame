var Gui = {};
Gui.setWindowFocus = function (obj) {
    // call the focus function on the active game object
    obj = $(obj);
    var objectId = obj.data('id');
    if (!obj.hasClass('focus')) {
        //Call the blur function on game objects of which the window is opened/visible
        $('.window').each(function (i, objW) {
            objW = $(objW);
            if (objW.is(":visible")) {
                var objectId = objW.data('id');
                if (objW.hasClass('truck')) {
                    Trucks.blurOverview(Trucks.trucks[objectId]);
                }
                else if (objW.hasClass('garage')) {
                    // Garages.blurOverview(Game.objects.Garages.garages[objectId]);
                }
            }
        });
        if (obj.hasClass('truck')) {
            Trucks.focusOverview(Trucks.trucks[objectId]);
        }
        else if (obj.hasClass('garage')) {
            // Garages.focusOverview(Game.objects.Garages.garages[objectId]);
        }
        // Set class accordingly on all window elements
        $('.window').removeClass('focus').zIndex(10);
        $(obj).addClass('focus').zIndex(11);
    }
};
/* Generic window functionality */
// Close window element
$('body').on('click', '.window .close', function (event) {
    event.stopPropagation();
    Template.destroy($(this).parents('.window'));
});
// Handle focus/blur of window elements
$('body').on('click', '.window', function () {
    Gui.setWindowFocus(this);
});
// Window tab functionality
$('body').on('click', '.window ul.tab-control li', function () {
    if (!$(this).hasClass("test")) {
        var obj = $(this);
        var index = obj.index();
        obj.parent().find('li').removeClass('active');
        obj.addClass('active');
        obj.parents('.window').find('.tab-wrapper li').removeClass('active');
        obj.parents('.window').find('.tab-wrapper > li').eq(index).addClass('active');
    }
});
