var Banks = (function () {
    function Banks(banksJson) {
        this.banks = [];
        this.view = null;
        // Initialize the banks
        var banksSettings = settings.banks;
        for (var i = banksSettings.length - 1; i >= 0; i--) {
            this.banks[i] = this.create(banksSettings[i]);
        }
    }
    Banks.prototype.create = function (bank) {
        for (var i = bank.loans.length - 1; i >= 0; i--) {
            bank.loans[i].available = i == 0 ? true : false;
            bank.loans[i].active = false;
            bank.loans[i].percentageDone = 0;
        }
        return { bank: bank };
    };
    Banks.prototype.setLoan = function (bank, loan) {
        var loan = this.banks[bank].bank.loans[loan];
        if (loan.active == false) {
            loan.active = true;
            loan.percentageDone = 0;
            // First payment date
            loan.started = Time.time;
            var now = new Date(Time.time * 1000);
            var loanStart = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
            var loanDone = new Date(loanStart.getFullYear(), loanStart.getMonth() + loan.duration, loanStart.getDate());
            var timeDiff = Math.abs(loanStart.getTime() - loanDone.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            loan.start = loanStart;
            loan.done = loanDone;
            loan.nextPayment = loanStart.getTime();
            loan.paymentPerDay = (loan.amount * (1 + (loan.interest / 100))) / diffDays;
            loan.day = 1;
            loan.totalDays = diffDays;
            // add funds
            Financial.addPlayerMoney(loan.amount);
        }
    };
    Banks.prototype.updateDay = function () {
        for (var i = this.banks.length - 1; i >= 0; i--) {
            var bank = this.banks[i].bank;
            // Update loans
            for (var li = this.banks[i].bank.loans.length - 1; li >= 0; li--) {
                var loan = this.banks[i].bank.loans[li];
                var now = Time.time * 1000;
                if (loan.active && now > loan.nextPayment) {
                    var nextPaymentDate = new Date(loan.nextPayment);
                    loan.nextPayment = new Date(nextPaymentDate.getFullYear(), nextPaymentDate.getMonth(), nextPaymentDate.getDate() + 1).getTime();
                    loan.day++;
                    loan.percentageDone = Math.floor((loan.day / loan.totalDays) * 100);
                    Financial.subtractPlayerMoney(loan.paymentPerDay);
                    if (loan.day > loan.totalDays) {
                        loan.active = false;
                        loan.percentageDone = 0;
                    }
                }
            }
        }
    };
    Banks.prototype.openOverview = function () {
        if ($('#bank').is(":hidden") || $('#bank').length == 0) {
            // Bank.openOverview();
            if ($('#bank').length == 0) {
                var windowEl = Template.create('bank');
                // windowEl.resizable({ maxHeight: 1000, maxWidth: 1000, minHeight: 250, minWidth: 550 });
                // windowEl.draggable({ start: function (event, ui) { UI.setWindowFocus(ui.helper[0]); }, handle: ".topbar", snap: ".window", snapMode: "outer" });
                $('body').append(windowEl);
                Game.objects.Gui.setWindowFocus(windowEl);
            }
            else if ($('#bank').is(":hidden")) {
                Game.objects.Gui.setWindowFocus('#bank');
            }
            else {
                Game.objects.Gui.setWindowFocus('#bank');
            }
            this.view = new Vue({
                el: '#bank',
                data: Game.objects.Banks,
                methods: {
                    setLoan: function (bank, loan, loanNum) {
                        if (loan.available) {
                            Game.objects.Banks.setLoan(bank, loanNum);
                        }
                    }
                }
            });
        }
        else {
            //unbind.bind($('#bank'))
            Template.destroy($('#bank'));
        }
    };
    return Banks;
}());
/*var Bank = {};
Bank.windowFocus = 0;
Bank.currentLoans = [];

Bank.updateBank = function(){

}

Bank.drawBank = function(){
    if($('#bank').is(":visible")){
        Bank.updateOverview();
    }
}

Bank.openOverview = function(){
    if($('#bank').length == 0){
        var bank = {};
        
        var windowEl = Template.create('bank', bank);
        windowEl.resizable({maxHeight: 1000, maxWidth: 1000, minHeight: 250, minWidth: 550});
        windowEl.draggable({start: function(event, ui){UI.setWindowFocus(ui.helper[0]);}, handle: ".topbar", snap: ".window", snapMode: "outer" });
        $('body').append(windowEl);
        UI.setWindowFocus(windowEl);

    } else if($('#bank').is(":hidden")){
        UI.setWindowFocus('#bank');
    } else {
        UI.setWindowFocus('#bank');
    }
}

// Update statistisc to active garage windows
Bank.updateOverview = function(){
    if($('#bank').length != 0){
        data = {};

        var currentLoans = '';
        if(Bank.currentLoans.length > 0) {
            for(var loan in Bank.currentLoans){
                loanName = '';
                currentLoans += '<div class="loan">'+loanName+'</div>';
            }
        } else {

        }
        data['currentLoans'] = currentLoans;

        var availableLoans = '';
        for(var bankObj in settings.loans.banks){
                availableLoans += '<div class="bank">bank name is '+bankObj.name+'</div>';

            // for (i = 0; i < garage.staff.length; i++) {
            // 	staffMember = garage.staff[i];
            // 	hiredStaff += '<div class="person"><b>'+staffMember.name+'</b><br>age: '+staffMember.age+'<br>sex: '+staffMember.gender+'<br>experience: '+staffMember.experience+' years<br>pay per month: '+Formatting.money(staffMember.pay)+'<br><br></div>';
            // }
        }
        data['availableLoans'] = availableLoans;

        for(var attribute in garage){
                data[attribute] = garage[attribute];
        }

        Template.parse($('#bank'), data);
    }
}

Bank.focusOverview = function(){
    Bank.windowFocus = 1;
}

Bank.blurOverview = function(){
    Bank.windowFocus = 0;
}

Bank.drawOverview = function(){
    // for (garage of Garages.garages) {
    // 	if($('.garage_'+garage.id).length){
    // 		Garages.updateOverview(garage);
    // 	}
    // }
}

Bank.destroyOverview = function(){
    Bank.windowFocus = 0;
}



Bank.getLoan = function(loan) {

}*/
