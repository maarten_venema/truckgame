var Time = {};
Time.time = settings.time;

// Update time
Time.updateTime = function() {
	Time.time += settings.timePerFrame;
	// Game.objects.Stats.time = Time.time;
}

Time.addDay = function() {
	Time.time += 60*60*24;
}

Time.addYear = function() {
	for (var i = 365 - 1; i >= 0; i--) {
		setTimeout(function(){
 			Time.addDay();
		}, 50*i);
	}
}