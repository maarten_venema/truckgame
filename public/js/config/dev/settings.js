var settings = {

	// General settings
	// Frames per second (amount of updates/steps per second)
	fps				: 12,

	// Time
	time 			: 1483228800, // Unix timestamp
	timePerCycle	: 180, // (60 default) Time in seconds per full fps cycle
	avgMeterPerStep : 18.8, // 9.4

	// Break and sleep settings in hours
	sleepInterval	: 9,
	sleepDuration	: 0.25,
	// sleepDuration	: 9,
	breakInterval	: 2,
	breakDuration	: 0.25,

	// center mainmap on init on these coordinates
	lat				: 52.3167,
	lng				: 5.5500,

	// the valutasign to be used
	locale			: 'nl-NL',
	moneyFormat		: 'EUR',

	// Starting money
	startingPlayerMoney	: 0,

	// Garage types
	/*
		name : 		 the initial name of the garage
		icon : 		 the garage icon on the map
		price : 		 initial build price of the garage
		upkeep : 	 upkeep price per month
		truckSpace : amount of trucks this garage can store
	*/
	garageTypes : [
					{
						name		: 'Small garage',
						icon 		: '/img/icons/map-garage.png',
						price 	 	: 150000,
						upkeep 		: 2500,
						truckSpace 	: 5
					},
					{
						name 		: 'Medium garage',
						icon 		: '/img/icons/map-garage2.png',
						price 	 	: 300000,
						upkeep 		: 4000,
						truckSpace 	: 15
					},
					{
						name 		: 'Big garage',
						icon 		: '/img/icons/map-garage3.png',
						price 	 	: 600000,
						upkeep 		: 7000,
						truckSpace 	: 30
					},
					{
						name 		: 'Huge garage',
						icon 		: '/img/icons/map-garage4.png',
						price 	 	: 1200000,
						upkeep 		: 20000,
						truckSpace 	: 50
					}
				],

	// Default truck object
	truck : {
				health			: 100,
				sleepTime		: null,
				breakTime		: null,
				wakeUpTime		: null,
				state			: 'waiting for job',
				prevState		: 'waiting for job',
				metersDriven	: 0, // km driven in total
				metersDrivenJob	: 0, // km driven on current job
				timeDriven		: 0, // in seconds
				routeStep		: 0,
				meterPerStep	: 0,
				direction 		: null,
				history			: {jobs:[], profit:0, costs:0},

				// Icons for diffrent states				
				icon 				: '/img/icons/map-truck.png',
				iconWaiting			: '/img/icons/map-truck-waiting.png',
				iconUnloading		: '/img/icons/map-truck-unloading.png',
				iconDriveBack		: '/img/icons/map-truck-driving-back.png',
				iconUnloading		: '/img/icons/map-truck-unloading.png',
				iconBreak			: '/img/icons/map-truck-break.png',
				iconRepair			: '/img/icons/map-truck-repair.png',
				iconBoat			: '/img/icons/map-boat.png'
			},

	// Truck types
	truckTypes : {
					DAF:[
						{
							type			: 'XF EURO 6',
							price 			: 100000,
							speed 			: 89,
							speedMax		: 95,
							// range 			: 100,
							range 			: 100,
							fuelType		: 'Diesel',
							fuelCapicity	: 500,
							fuelCapicityMax	: 2000,
							fuelEfficiency 	: 3.5,
							cargo			: 30,
							cargoMax		: 40,
							cargoTypes		: ['food'],
							upgrades		: {
								cargoTypes: {
									food: {
										value: 'food',
										description: 'Food',
										cost: 2000,
										active: 1
									},
									flammable: {
										value: 'flammable',
										description: 'Flammable goods',
										cost: 10000,
										active: 0
									},
									explosive: {
										value: 'explosive',
										description: 'Explosive goods',
										cost: 10000,
										active: 0
									},
									chemical: {
										value: 'chemical',
										description: 'Chemicals',
										cost: 10000,
										active: 0
									},
								},
								range: {
									rangeUpgr1: {
										value: 200,
										description: 200,
										cost: 1000,
										active: 0
									},
									rangeUpgr2: {
										value: 400,
										description: 400,
										cost: 2000,
										active: 0
									},
									rangeUpgr3: {
										value: 800,
										description: 800,
										cost: 4000,
										active: 0
									},
									rangeUpgr4: {
										value: 1600,
										description: 1600,
										cost: 8000,
										active: 0
									}
								}
							}
						},
						{
							type			: 'XF105',
							price 			: 80000,
							speed			: 85,
							speedMax		: 89,
							// range 			: 100,
							range 			: 100,
							fuelType		: 'Diesel',
							fuelCapicity	: 700,
							fuelCapicityMax	: 1200,
							fuelEfficiency 	: 3.5,
							cargo			: 10,
							cargoMax		: 15,
							cargoTypes		: ['food'],
							upgrades		: {
								cargoTypes: {
									food: {
										value: 'food',
										description: 'Food',
										cost: 1000,
										active: 1
									},
									flammable: {
										value: 'flammable',
										description: 'Flammable goods',
										cost: 1000,
										active: 0
									}
								},
								range: {
									rangeUpgr1: {
										value: 200,
										description: 200,
										cost: 1000,
										active: 0
									},
									rangeUpgr2: {
										value: 400,
										description: 400,
										cost: 2000,
										active: 0
									},
									rangeUpgr3: {
										value: 800,
										description: 800,
										cost: 4000,
										active: 0
									},
									rangeUpgr4: {
										value: 1600,
										description: 1600,
										cost: 8000,
										active: 0
									}
								}
							}
						}
					],
					MAN:[
						{
							type			: 'VTK 2300',
							price 			: 100000,
							speed 			: 95,
							speedMax		: 95,
							// range 			: 100,
							range 			: 100,
							fuelType		: 'Diesel',
							fuelCapicity	: 600,
							fuelCapicityMax	: 1200,
							fuelEfficiency 	: 3.5,
							cargo			: 20,
							cargoMax		: 35,
							cargoTypes		: ['food'],
							upgrades		: {
								cargoTypes: {
									food: {
										value: 'food',
										description: 'Food',
										cost: 100,
										active: 1
									},
									flammable: {
										value: 'flammable',
										description: 'Flammable goods',
										cost: 100,
										active: 0
									},
									chemical: {
										value: 'chemical',
										description: 'Chemicals',
										cost: 100,
										active: 0
									}
								},
								range: {
									rangeUpgr1: {
										value: 200,
										description: 200,
										cost: 1000,
										active: 0
									},
									rangeUpgr2: {
										value: 400,
										description: 400,
										cost: 2000,
										active: 0
									},
									rangeUpgr3: {
										value: 800,
										description: 800,
										cost: 4000,
										active: 0
									},
									rangeUpgr4: {
										value: 1600,
										description: 1600,
										cost: 8000,
										active: 0
									}
								}
							}
						}
					]
				},

	// Trailer types
	// (not used atm)
	// trailerTypes : {
	// 				fluid:[
	// 					{
	// 						type 	: 'NAME',
	// 						price 	: 40000,
	// 						weight	: 7,
	// 						cargo 	: 25000,
	// 						cargoTypes		: ['food', 'nonFoodLowValue', 'nonFoodMidValue']
	// 					},
	// 					{
	// 						type 	: 'NAME',
	// 						price 	: 60000,
	// 						weight	: 8,
	// 						cargo 	: 30000,
	// 						cargoTypes		: ['food', 'nonFoodLowValue', 'nonFoodMidValue']
	// 					}
	// 				],
	// 				fluid:[
	// 					{
	// 						type 	: 'NAME',
	// 						price 	: 40000,
	// 						weight	: 7,
	// 						cargo 	: 36000,
	// 						cargoTypes		: ['fluids']
	// 					}
	// 				]
	// 			},

	/*
		Cargo types
		Includes some experimental values which will or will nog be used depending on tests and future features
		price = /ton/km
	*/
	cargoTypes : {
		food : [
				{name: 'Potatoes', price: 0.02, density: 770, weight: 770}, 
				{name: 'Onions', price: 0.02, density: 770, weight: 770},
				{name: 'Beets', price: 0.02, density: 770, weight: 770},
				{name: 'Sugar', price: 0.02, density: 770, weight: 770},
				{name: 'Carrots', price: 0.02, density: 770, weight: 770},
				{name: 'Celery', price: 0.02, density: 770, weight: 770},
				{name: 'White beans', price: 0.02, density: 770, weight: 770},
				{name: 'Broccolini', price: 0.02, density: 770, weight: 770},
				{name: 'Lettuce', price: 0.02, density: 770, weight: 770},
				{name: 'Mustard', price: 0.02, density: 770, weight: 770},
				{name: 'Spinach', price: 0.02, density: 770, weight: 770},
				{name: 'Avocado', price: 0.02, density: 770, weight: 770},
		],		
		flammable : [
				{name: 'Acetone', price: 0.8, density: 770, weight: 770},
				{name: 'Diesel fuel', price: 0.8, density: 770, weight: 770},
				{name: 'Petroleum distillates', price: 0.8, density: 770, weight: 770},
				{name: 'Kerosene', price: 0.8, density: 770, weight: 770},
				{name: 'Ethanol', price: 0.8, density: 770, weight: 770},
				{name: 'Benzene', price: 0.8, density: 770, weight: 770},
		],		
		explosive : [
				{name: 'Flares', price: 1.2, density: 770, weight: 770},
				{name: 'Igniters', price: 1.2, density: 770, weight: 770},
				{name: 'Rockets', price: 1.2, density: 770, weight: 770},
				{name: 'TNT', price: 1.2, density: 770, weight: 770},
				{name: 'RDX', price: 1.2, density: 770, weight: 770},
				{name: 'PETN', price: 1.2, density: 770, weight: 770},
		],
		chemical : [
				{name: 'Nitrites', price: 1.2, density: 770, weight: 770},
				{name: 'Potassium nitrate', price: 1.2, density: 770, weight: 770},
				{name: 'Sodium nitrate', price: 1.2, density: 770, weight: 770},
				{name: 'Potassium chlorate', price: 1.2, density: 770, weight: 770},
				{name: 'Magnesium peroxide', price: 1.2, density: 770, weight: 770},
				{name: 'Sodium persulphate', price: 1.2, density: 770, weight: 770},
		]
	},
	
	// Available loans
	banks : [
		{
			name:'TT Bank', 
			loans : [
				{
					name: 'small loan', 
					amount: 500000, 
					interest: 12, 
					duration: 6,
					moneyNeeded: '0',
					desc: 'small starter loan'
				},
				{
					name: 'medium loan', 
					amount: 1000000, 
					interest: 14, 
					duration: 12,
					moneyNeeded: '0',
					desc: 'You need to make atleast 20000,- a month for this loan to become available'
				},
				{
					name: 'big loan', 
					amount: 1500000, 
					interest: 13, 
					duration: 18,
					moneyNeeded: '0',
					desc: 'You need to make atleast 30000,- a month for this loan to become available'
				}
			]
		},
		{
			name:'Nederlandse Giro Bank', 
			loans : [
				{
					name: 'small loan', 
					amount: 800000, 
					interest: 13, 
					duration: 12,
					moneyNeeded: '0',
					desc: 'You need to make atleast 5000,- a month for this loan to become available'
				},
				{
					name: 'medium loan', 
					amount: 1400000, 
					interest: 15, 
					duration: 12,
					moneyNeeded: '0',
					desc: 'You need to make atleast 20000,- a month for this loan to become available'
				},
				{
					name: 'big loan', 
					amount: 2000000, 
					interest: 15, 
					duration: 14,
					moneyNeeded: '0',
					desc: 'You need to make atleast 30000,- a month for this loan to become available'
				}
			]
		}
	],

	// Generic marker icons 
	markerIcons : {
		delivery : '/img/icons/delivery.png'
	}
	
};
/* Computed settings */
// Seconds to add to the in game time per frame. Currently 1 second (30 frames) = 1 minute
settings.timePerFrame = settings.timePerCycle/settings.fps;