var gdppcap = {
  "ABW":"25354,78247",
  "AND":"46418,41547",
  "AFG":"664,7645892",
  "AGO":"5783,36676",
  "ALB":"4460,340971",
  "ARB":"7715,977809",
  "ARE":"43048,85015",
  "ARG":"14715,18002",
  "ARM":"3504,766758",
  "ASM":"",
  "ATG":"13342,085",
  "AUS":"67463,02193",
  "AUT":"50510,71203",
  "AZE":"7811,621418",
  "BDI":"267,1093219",
  "BEL":"46929,63546",
  "BEN":"804,6924986",
  "BFA":"760,8529512",
  "BGD":"957,824266",
  "BGR":"7498,831484",
  "BHR":"24689,10563",
  "BHS":"22312,08297",
  "BIH":"4661,764245",
  "BLR":"7575,48211",
  "BLZ":"4893,926635",
  "BMU":"84470,75527",
  "BOL":"2867,639791",
  "BRA":"11208,08274",
  "BRB":"14917,14951",
  "BRN":"38563,31451",
  "BTN":"2362,581737",
  "BWA":"7315,019289",
  "CAF":"333,1968806",
  "CAN":"51964,33023",
  "CEB":"13605,75633",
  "CHE":"84748,36531",
  "CHI":"73577,16661",
  "CHL":"15732,31377",
  "CHN":"6807,430824",
  "CIV":"1528,937539",
  "CMR":"1328,640205",
  "COG":"3167,045322",
  "COL":"7831,215313",
  "COM":"814,9571488",
  "CPV":"3767,115364",
  "CRI":"10184,60567",
  "CSS":"9572,858472",
  "CUB":"6051,222001",
  "CUW":"",
  "CYM":"30190,66769",
  "CYP":"25248,98107",
  "CZE":"19858,34346",
  "DEU":"46251,3818",
  "DJI":"1668,336703",
  "DMA":"7175,626941",
  "DNK":"59818,63153",
  "DOM":"5878,996721",
  "DZA":"5360,701146",
  "EAP":"5690,320887",
  "EAS":"9116,159502",
  "ECA":"7347,539848",
  "ECS":"25484,64309",
  "ECU":"6002,885459",
  "EGY":"3314,462928",
  "EMU":"39116,30051",
  "ERI":"543,8219083",
  "ESP":"29882,13579",
  "EST":"18877,33049",
  "ETH":"505,0457458",
  "EUU":"35416,87843",
  "FCS":"1598,431238",
  "FIN":"49150,5773",
  "FJI":"4375,406022",
  "FRA":"42560,41373",
  "FRO":"44317,30589",
  "FSM":"3054,068122",
  "GAB":"11571,08292",
  "GBR":"41781,14902",
  "GEO":"3596,90832",
  "GHA":"1858,242598",
  "GIN":"523,1190322",
  "GMB":"488,5655946",
  "GNB":"563,7527714",
  "GNQ":"20581,60594",
  "GRC":"21965,92677",
  "GRD":"7890,513319",
  "GRL":"22507,88872",
  "GTM":"3477,890061",
  "GUM":"",
  "GUY":"3739,469995",
  "HIC":"39108,39103",
  "HKG":"38123,52212",
  "HND":"2290,780533",
  "HPC":"889,3405969",
  "HRV":"13597,92145",
  "HTI":"819,9039143",
  "HUN":"13485,47207",
  "IDN":"3475,250474",
  "IMN":"49817,44469",
  "IND":"1497,549864",
  "INX":"",
  "IRL":"50478,41033",
  "IRN":"4763,303309",
  "IRQ":"6862,495681",
  "ISL":"47349,48255",
  "ISR":"36050,69793",
  "ITA":"35685,59904",
  "JAM":"5290,486134",
  "JOR":"5213,390116",
  "JPN":"38633,70806",
  "KAZ":"13611,53736",
  "KEN":"1245,512041",
  "KGZ":"1263,428083",
  "KHM":"1006,839744",
  "KIR":"1650,707224",
  "KNA":"14132,80188",
  "KOR":"25976,95283",
  "KWT":"52197,34134",
  "LAC":"9621,065726",
  "LAO":"1660,706031",
  "LBN":"9928,038098",
  "LBR":"454,3374834",
  "LBY":"11964,7307",
  "LCA":"7328,370692",
  "LCN":"10008,05256",
  "LDC":"912,8845641",
  "LIC":"741,8807125",
  "LIE":"134617,3796",
  "LKA":"3279,89139",
  "LMC":"2043,104822",
  "LMY":"4223,756955",
  "LSO":"1125,586427",
  "LTU":"15529,68161",
  "LUX":"110664,8403",
  "LVA":"15381,08353",
  "MAC":"91376,02254",
  "MAF":"",
  "MAR":"3092,606545",
  "MCO":"163025,859",
  "MDA":"2239,559127",
  "MDG":"462,9689428",
  "MDV":"6665,767695",
  "MEA":"8550,131406",
  "MEX":"10307,28304",
  "MHL":"3627,210548",
  "MIC":"4816,870194",
  "MKD":"4838,462105",
  "MLI":"715,133813",
  "MLT":"22774,96038",
  "MMR":"",
  "MNA":"4329,663089",
  "MNE":"7106,861774",
  "MNG":"4056,397839",
  "MNP":"",
  "MOZ":"605,0341744",
  "MRT":"1068,974597",
  "MUS":"9477,791587",
  "MWI":"226,4551027",
  "MYS":"10538,05789",
  "NAC":"52940,44736",
  "NAM":"5693,129154",
  "NCL":"",
  "NER":"415,4173218",
  "NGA":"3005,513796",
  "NIC":"1851,105852",
  "NLD":"50792,51426",
  "NOC":"21330,37374",
  "NOR":"100898,3615",
  "NPL":"694,1047943",
  "NZL":"41824,32284",
  "OEC":"43387,12701",
  "OED":"38020,7979",
  "OMN":"21929,01457",
  "OSS":"4867,984384",
  "PAK":"1275,301817",
  "PAN":"11036,80739",
  "PER":"6661,591112",
  "PHL":"2765,084587",
  "PLW":"11810,08701",
  "PNG":"2105,269876",
  "POL":"13653,72163",
  "PRI":"28528,9971",
  "PRK":"",
  "PRT":"21738,2916",
  "PRY":"4264,650642",
  "PSS":"3512,726275",
  "PYF":"",
  "QAT":"93714,06338",
  "ROU":"9490,754433",
  "RUS":"14611,70078",
  "RWA":"638,6657954",
  "SAS":"1417,449221",
  "SAU":"25961,80842",
  "SDN":"1753,38091",
  "SEN":"1046,586426",
  "SGP":"55182,48279",
  "SLB":"1953,557318",
  "SLE":"678,9609045",
  "SLV":"3826,082486",
  "SMR":"62188,92859",
  "SOM":"",
  "SRB":"6353,826383",
  "SSA":"1755,243107",
  "SSD":"1044,995162",
  "SSF":"1770,620874",
  "SST":"5881,287625",
  "STP":"1609,823339",
  "SUR":"9825,743921",
  "SVK":"18049,18346",
  "SVN":"23295,33909",
  "SWE":"60380,94801",
  "SWZ":"3034,223184",
  "SXM":"",
  "SYC":"16185,89948",
  "SYR":"2065,539632",
  "TCA":"",
  "TCD":"1053,662501",
  "TGO":"636,43645",
  "THA":"5778,977216",
  "TJK":"1036,583276",
  "TKM":"7986,698884",
  "TLS":"1105,349369",
  "TON":"4426,944581",
  "TTO":"18372,90432",
  "TUN":"4316,685695",
  "TUR":"10971,65631",
  "TUV":"3880,352322",
  "TZA":"912,7003097",
  "UGA":"657,3706635",
  "UKR":"3900,465376",
  "UMC":"7763,940896",
  "URY":"16350,72817",
  "USA":"53041,98141",
  "UZB":"1877,964512",
  "VCT":"6485,679146",
  "VEN":"14414,75353",
  "VIR":"",
  "VNM":"1910,512818",
  "VUT":"3276,734258",
  "PSE":"2782,905026",
  "WLD":"10613,45009",
  "WSM":"4212,363465",
  "YEM":"1473,099564",
  "ZAF":"6886,290405",
  "COD":"484,2114713",
  "ZMB":"1844,799139",
  "ZWE":"953,3806071"
}