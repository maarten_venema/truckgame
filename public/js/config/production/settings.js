var settings = {

	// General settings
	// Frames per second (amount of updates/steps per second)
	fps				: 30,

	// Time
	time 			: 1420113600, // Unix timestamp
	timePerCycle	: 60, // Time in seconds per full fps cycle
	avgMeterPerStep : 9.4,

	// Break and sleep settings in hours
	sleepInterval	: 9,
	sleepDuration	: 9,
	breakInterval	: 2,
	breakDuration	: 0.25,

	// center mainmap on init on these coordinates
	lat				: 52.3167,
	lng				: 5.5500,

	// the valutasign to be used
	locale			: 'nl-NL',
	moneyFormat		: 'EUR',

	// Starting money
	startingPlayerMoney	: 5000000,

	// Garage types
	/*
		name : 		 the initial name of the garage
		icon : 		 the garage icon on the map
		price : 		 initial build price of the garage
		upkeep : 	 upkeep price per month
		truckSpace : amount of trucks this garage can store
	*/
	GarageTypes : [
					{
						name		: 'Small garage',
						icon 		: '/img/icons/map-garage.png',
						price 	 	: 150000,
						upkeep 		: 1000,
						truckSpace 	: 5
					},
					{
						name 		: 'Medium garage',
						icon 		: '/img/icons/map-garage2.png',
						price 	 	: 300000,
						upkeep 		: 2000,
						truckSpace 	: 15
					},
					{
						name 		: 'Big garage',
						icon 		: '/img/icons/map-garage3.png',
						price 	 	: 600000,
						upkeep 		: 4000,
						truckSpace 	: 30
					},
					{
						name 		: 'Huge garage',
						icon 		: '/img/icons/map-garage4.png',
						price 	 	: 1200000,
						upkeep 		: 8000,
						truckSpace 	: 50
					}
				],

	// Default truck object
	truck : {
				health			: 100,
				sleepTime		: null,
				breakTime		: null,
				wakeUpTime		: null,
				state			: 'waiting for job',
				prevState		: 'waiting for job',
				metersDriven	: 0, // km driven in total
				metersDrivenJob	: 0, // km driven on current job
				timeDriven		: 0, // in seconds
				routeStep		: 0,
				meterPerStep	: 0,
				direction 		: null,
				history			: {jobs:[], profit:0, costs:0},

				// Icons for diffrent states				
				icon 				: '/img/icons/map-truck.png',
				iconWaiting			: '/img/icons/map-truck-waiting.png',
				iconUnloading		: '/img/icons/map-truck-unloading.png',
				iconDriveBack		: '/img/icons/map-truck-driving-back.png',
				iconUnloading		: '/img/icons/map-truck-unloading.png',
				iconBreak			: '/img/icons/map-truck-break.png',
				iconRepair			: '/img/icons/map-truck-repair.png'
			},

	// Truck types
	truckTypes : {
					DAF:[
						{
							type			: 'XF EURO 6',
							price 			: 100000,
							speed 			: 89,
							speedMax		: 95,
							range 			: 1000,
							rangeMax		: 2000,
							fuelType		: 'Diesel',
							fuelCapicity	: 1000,
							fuelCapicityMax	: 2000,
							fuelEfficiency 	: 30,
							cargo			: 30,
							cargoMax		: 40,
							cargoTypes		: ['food'],
							cargoTypesUpgr	: ['food','flammable','explosive','chemical']
						},
						{
							type			: 'XF105',
							price 			: 80000,
							speed			: 85,
							speedMax		: 89,
							range 			: 600,
							rangeMax		: 1000,
							fuelType		: 'Diesel',
							fuelCapicity	: 700,
							fuelCapicityMax	: 1200,
							fuelEfficiency 	: 28,
							cargo			: 10,
							cargoMax		: 15,
							cargoTypes		: ['food'],
							cargoTypesUpgr	: ['food','chemical']
						}
					],
					MAN:[
						{
							type			: 'VTK 2300',
							price 			: 100000,
							speed 			: 95,
							speedMax		: 95,
							range 			: 1000,
							rangeMax		: 1500,
							fuelType		: 'Diesel',
							fuelCapicity	: 600,
							fuelCapicityMax	: 1200,
							fuelEfficiency 	: 29,
							cargo			: 20,
							cargoMax		: 35,
							cargoTypes		: ['food'],
							cargoTypesUpgr	: ['food','flammable','chemical']
						}
					]
				},

	// Trailer types
	// (not used atm)
	// trailerTypes : {
	// 				fluid:[
	// 					{
	// 						type 	: 'NAME',
	// 						price 	: 40000,
	// 						weight	: 7,
	// 						cargo 	: 25000,
	// 						cargoTypes		: ['food', 'nonFoodLowValue', 'nonFoodMidValue']
	// 					},
	// 					{
	// 						type 	: 'NAME',
	// 						price 	: 60000,
	// 						weight	: 8,
	// 						cargo 	: 30000,
	// 						cargoTypes		: ['food', 'nonFoodLowValue', 'nonFoodMidValue']
	// 					}
	// 				],
	// 				fluid:[
	// 					{
	// 						type 	: 'NAME',
	// 						price 	: 40000,
	// 						weight	: 7,
	// 						cargo 	: 36000,
	// 						cargoTypes		: ['fluids']
	// 					}
	// 				]
	// 			},

	/*
		Cargo types
		Includes some experimental values im still trying out
	*/
	cargoTypes : {
		food : [
				{name: 'food', upgradeCost: 5000},
				{name: 'Potatoes', price: 90, density: 770, weight: 770}, 
				{name: 'Onions', price: 90, density: 770, weight: 770},
				{name: 'Beets', price: 90, density: 770, weight: 770},
				{name: 'Sugar', price: 90, density: 770, weight: 770},
				{name: 'Carrots', price: 90, density: 770, weight: 770},
				{name: 'Celery', price: 90, density: 770, weight: 770},
				{name: 'White beans', price: 90, density: 770, weight: 770},
				{name: 'Broccolini', price: 90, density: 770, weight: 770},
				{name: 'Lettuce', price: 90, density: 770, weight: 770},
				{name: 'Mustard', price: 90, density: 770, weight: 770},
				{name: 'Spinach', price: 90, density: 770, weight: 770},
				{name: 'Avocado', price: 90, density: 770, weight: 770},
		],		
		flammable : [
				{name: 'flammable', upgradeCost: 6000},
				{name: 'Acetone', price: 100, density: 770, weight: 770},
				{name: 'Diesel fuel', price: 100, density: 770, weight: 770},
				{name: 'Petroleum distillates', price: 100, density: 770, weight: 770},
				{name: 'Kerosene', price: 100, density: 770, weight: 770},
				{name: 'Ethanol', price: 100, density: 770, weight: 770},
				{name: 'Benzene', price: 100, density: 770, weight: 770},
		],		
		explosive : [
				{name: 'explosive', upgradeCost: 7000},
				{name: 'Flares', price: 120, density: 770, weight: 770},
				{name: 'Igniters', price: 120, density: 770, weight: 770},
				{name: 'Rockets', price: 120, density: 770, weight: 770},
				{name: 'TNT', price: 120, density: 770, weight: 770},
				{name: 'RDX', price: 120, density: 770, weight: 770},
				{name: 'PETN', price: 120, density: 770, weight: 770},
		],
		chemical : [
				{name: 'chemical', upgradeCost: 8000},
				{name: 'Nitrites', price: 120, density: 770, weight: 770},
				{name: 'Potassium nitrate', price: 120, density: 770, weight: 770},
				{name: 'Sodium nitrate', price: 120, density: 770, weight: 770},
				{name: 'Potassium chlorate', price: 120, density: 770, weight: 770},
				{name: 'Magnesium peroxide', price: 120, density: 770, weight: 770},
				{name: 'Sodium persulphate', price: 120, density: 770, weight: 770},
		]
	},
	
	// Generic marker icons 
	markerIcons : {
		delivery : '/img/icons/delivery.png'
	}
	
};
/* Computed settings */
// Seconds to add to the in game time per frame. Currently 1 second (30 frames) = 1 minute
settings.timePerFrame = settings.timePerCycle/settings.fps;

// Generate the upgrades available for all truck types and a dom representation
function generateTruckUpgrades(){
	for(var truckNum in settings.truckTypes){
		truckTypes = settings.truckTypes[truckNum];

		for (i = 0; i < truckTypes.length; i++) { 
    		settings.truckTypes[truckNum][i].upgrades = {};

    		// Generate ADR upgrades
    		settings.truckTypes[truckNum][i].upgrades.ADR = {};
    		var adrDom='';
    		var adrUpgradeState=[];
    		for(var adrNum in settings.truckTypes[truckNum][i].cargoTypesUpgr){
    			var adrType = settings.truckTypes[truckNum][i].cargoTypesUpgr[adrNum];
    			if(settings.truckTypes[truckNum][i].cargoTypes.indexOf(settings.truckTypes[truckNum][i].cargoTypesUpgr[adrNum])>-1){
	    			adrDom += '<li class="upgrade activated" data-upgrade-type="ADR" data-upgrade-num="'+adrNum+'">'+adrType+'</li>';
	    			adrUpgradeState.push({'upgrade':adrType, 'state':true});
	    		} else {
	    			adrDom += '<li class="upgrade" data-upgrade-type="ADR" data-upgrade-num="'+adrNum+'">'+adrType+'</li>';
	    			adrUpgradeState.push({'upgrade':adrType, 'state':false});
	    		}    		
			}

			settings.truckTypes[truckNum][i].upgrades.ADR.dom = adrDom;
			settings.truckTypes[truckNum][i].upgrades.ADR.state = adrUpgradeState;

    		// Generate range updates
    		settings.truckTypes[truckNum][i].upgrades.range = {};
    		var rangeDom='';
    		var rangeUpgradeState=[];
    		var numOfUpgrades = (settings.truckTypes[truckNum][i].rangeMax - settings.truckTypes[truckNum][i].range)/100;
    		for (i2 = 0; i2 < numOfUpgrades; i2++) { 
    			rangeDom += '<li class="upgrade" data-upgrade-type="range" data-upgrade-num="'+i2+'">add 100 km</li>';
    			rangeUpgradeState.push({'upgrade':'100 km', 'state':false});
    		}
    		
    		settings.truckTypes[truckNum][i].upgrades.range.dom = rangeDom;
    		settings.truckTypes[truckNum][i].upgrades.range.state = rangeUpgradeState;
		}
	}
}

generateTruckUpgrades();