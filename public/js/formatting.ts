var Formatting = {};

Formatting.moneyFormatter = new Intl.NumberFormat(settings.locale, {
  style: 'currency',
  currency: settings.moneyFormat,
  minimumFractionDigits: 0,
  maximumFractionDigits: 0
});

Formatting.numberFormatterNoDecimals = new Intl.NumberFormat(settings.locale, {
  style: 'decimal',
  currency: settings.moneyFormat,
  minimumFractionDigits: 0,
  maximumFractionDigits: 0
});

Formatting.numberFormatterTwoDecimals = new Intl.NumberFormat(settings.locale, {
  style: 'decimal',
  currency: settings.moneyFormat,
  minimumFractionDigits: 2,
  maximumFractionDigits: 2
});

// var dateFormat = {}
// dateFormat.year = dateFormat.month = dateFormat.day = dateFormat.hour = dateFormat.minute = dateFormat.second = 'numeric';
var dateFormat = {
  day: '2-digit', month: '2-digit', year: 'numeric', hour: 'numeric', minute: 'numeric'
};

Formatting.timestampToDateFormatter = new Intl.DateTimeFormat(settings.locale, dateFormat);

var dateFormatNoTime = {
  day: '2-digit', month: '2-digit', year: 'numeric'
};

Formatting.timestampToDateFormatterNoTime = new Intl.DateTimeFormat(settings.locale, dateFormatNoTime);

Formatting.mToKm = function(meters){
    var returnVal = Formatting.numberFormatterNoDecimals.format(Math.round(meters/1000));
  // returnVal = (parseFloat(returnVal) < 1000) ? 0 : returnVal;
    return parseFloat(returnVal);
};

Formatting.money = function(value){
    var returnVal = Formatting.moneyFormatter.format(value);
    return returnVal;
};

Formatting.twoDigits = function(value){
    var returnVal = Formatting.numberFormatterTwoDecimals.format(value);
    return returnVal;
};

Formatting.noDigits = function(value){
  var returnVal = Formatting.numberFormatterNoDecimals.format(value);
  return parseFloat(returnVal);
};

Formatting.timestampToDate = function(timestamp){
  var returnVal = Formatting.timestampToDateFormatter(timestamp);
  return returnVal;
};

Vue.filter('formatDate', function(value) {
  if (value) {
    return Formatting.timestampToDateFormatter.format(value*1000);
  }
});

Vue.filter('formatDateNoTime', function(value) {
  if (value) {
    return Formatting.timestampToDateFormatterNoTime.format(value*1000);
  }
});

Vue.filter('money', function(value) {
  if (value) {
    return Formatting.moneyFormatter.format(value);
  }
});

Vue.filter('percentage', function(value) {
  if (value) {
    return value+'%';
  }
});