var express = require('express');
var path = require('path');
var app = express();

app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, '/public')));

app.use("/css",  express.static(__dirname + '/public/css'));
app.use("/js", express.static(__dirname + '/public/js'));
app.use("/img",  express.static(__dirname + '/public/img'));
app.use("/tmpl",  express.static(__dirname + '/public/templates'));
app.use("/node_modules",  express.static(__dirname + '/node_modules'));
app.use("/test",  express.static(__dirname + '/test'));

var http = require('http').Server(app);

app.get('/', function(req, res){
	res.sendFile('/public/index.html');
});

app.get('/test', function(req, res){
	res.sendFile(path.join(__dirname, '/test/')+'test.html');
});

http.listen(app.get('port'), function(){
	console.log('listening on *:'+app.get('port'));
});

